//la fonction abonnements permet l'assignation de gestionnaires d'évènements à un élément cible 
var abonnements = function() {
	
	var croix1 = document.getElementById("A1");
	croix1.addEventListener("click",A1X);
		
		var croix2 = document.getElementById("A2");
	croix2.addEventListener("click",A2X);
	
		var croix3 = document.getElementById("A3");
	croix3.addEventListener("click",A3X);
	
		var croix4 = document.getElementById("A4");
	croix4.addEventListener("click",B1X);
	
		var croix5 = document.getElementById("A5");
	croix5.addEventListener("click",B2X);
	
		var croix6 = document.getElementById("A6");
	croix6.addEventListener("click",B3X);
	
		var croix7 = document.getElementById("A7");
	croix7.addEventListener("click",C1X);
	
		var croix8 = document.getElementById("A8");
	croix8.addEventListener("click",C2X);
	
		var croix9 = document.getElementById("A9");
	croix9.addEventListener("click",C3X);
	
	var deb = document.getElementById("start");
	deb.addEventListener("click",go);
	
	var debb = document.getElementById("komen");
	debb.addEventListener("click",tuto2);
	
	var c = document.getElementById("pane").getContext('2d'); 
	var size = 80;
	

	c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.moveTo(200, 0);
	c.lineTo(200, 800);
	c.stroke();
	
		c.beginPath();
	c.moveTo(400, 0);
	c.lineTo(400, 800);
	c.stroke();
	
		c.beginPath();
	c.moveTo(0, 200);
	c.lineTo(800, 200);
	c.stroke();
			
	c.beginPath();
	c.moveTo(0, 400);
	c.lineTo(800, 400);
	c.stroke();
}
	 a1 = 0;
	 a2 = 0;
	 a3 = 0;
	 b1 = 0;
	 b2 = 0;
	 b3 = 0;
	 c1 = 0;
	 c2 = 0;
	 c3 = 0;
	 
	a1cx = 0;
	a2cx = 0;
	a3cx = 0;
	b1cx = 0;
	b2cx = 0;
	b3cx = 0; 
	c1cx = 0;
	c2cx = 0;
	c3cx = 0;
	a1xx = 0;
	a2xx = 0;
	a3xx = 0;
	b1xx = 0;
	b2xx = 0;
	b3xx = 0; 
	c1xx = 0;
	c2xx = 0;
	c3xx = 0;
	end = 0;
	
	var go = function(){
		var c = document.getElementById("pane").getContext('2d'); 
			document.getElementById("ki").innerHTML="<br>Vous commencez.";
		
		var c = document.getElementById("pane").getContext('2d');
		var h = c.canvas.height;
		var w = c.canvas.width;
		c.clearRect(0, 0, w, h);
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.moveTo(200, 0);
	c.lineTo(200, 800);
	c.stroke();
	
		c.beginPath();
	c.moveTo(400, 0);
	c.lineTo(400, 800);
	c.stroke();
	
		c.beginPath();
	c.moveTo(0, 200);
	c.lineTo(800, 200);
	c.stroke();
			
	c.beginPath();
	c.moveTo(0, 400);
	c.lineTo(800, 400);
	c.stroke();
	a1 = 1;
	a2 = 1;
	a3 = 1;
	b1 = 1;
	b2 = 1;
	b3 = 1;
	c1 = 1;
	c2 = 1;
	c3 = 1;
	
	a1cx = 0;
	a2cx = 0;
	a3cx = 0;
	b1cx = 0;
	b2cx = 0;
	b3cx = 0;
	c1cx = 0;
	c2cx = 0;
	c3cx = 0;
	a1xx = 0;
	a2xx = 0;
	a3xx = 0;
	b1xx = 0;
	b2xx = 0;
	b3xx = 0;
	c1xx = 0;
	c2xx = 0;
	c3xx = 0;
	
	end = 0;
	}

	var tuto2 = function(){
		document.getElementById("tuto").innerHTML="But du jeu: Dessiner une ligne de son symbole (croix ou cercles), cette ligne peut-être horizontale, verticale ou en diagonale. <br>Déroulement de la partie: Chaque joueur doit à tour de rôle placer un de ces symboles, en utilisant le pavé qui leur est destiné. <br>La partie se fini dés que l'un des joueurs a réalisé une ligne de 3 symboles identiques.";
	} 
	
	var A1C = function(){
	//A1cercle
	if(a1 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(100, 100, 90, 0, 2 * Math.PI);
	c.stroke();
	a1 = 0;
	a1cx = 1;
	fin();
	}}
	
	var A2C = function(){
	//A2cercle
	if(a2 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(300, 100, 90, 0, 2 * Math.PI);
	c.stroke();
	a2=0;
	a2cx = 1;
	fin();
	}}
	
	var A3C = function(){
	//A3cercle
	if(a3 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(500, 100, 90, 0, 2 * Math.PI);
	c.stroke();
	a3= 0;
	a3cx = 1;
	fin();
	}}
	
	var B1C = function(){
	//B1cercle
	if(b1 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(100, 300, 90, 0, 2 * Math.PI);
	c.stroke();
	b1= 0;
	b1cx = 1;
	fin();
	}
	}
	
	var B2C = function(){
	//B2cercle
	if(b2 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(300, 300, 90, 0, 2 * Math.PI);
	c.stroke();
	b2 = 0
	b2cx = 1;
	fin();
	}
	}
	
	var B3C = function(){
	//B3cercle
	if(b3 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(500, 300, 90, 0, 2 * Math.PI);
	c.stroke();
	b3 = 0;
	b3cx = 1;
	fin();
	}
	}
	
	var C1C = function(){
	//C1cercle
	if(c1 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(100, 500, 90, 0, 2 * Math.PI);
	c.stroke();
	c1 = 0;
	c1cx = 1;
	fin();
	}
	}
	
	var C2C = function(){
	//C2cercle
	if(c2 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(300, 500, 90, 0, 2 * Math.PI);
	c.stroke();
	c2 = 0;
	c2cx = 1;
	fin();
	}
	}
	
	var C3C = function(){
	//C3cercle
	if(c3 == 1){
	var c = document.getElementById("pane").getContext('2d'); 
		c.beginPath();
	c.strokeStyle = " #cb4335 ";
	c.lineWidth="1";
	c.arc(500, 500, 90, 0, 2 * Math.PI);
	c.stroke();
	c3 = 0;
	c3cx = 1;
	fin();
	}
	}
	
	var A1X = function(){
		//A1
		if(a1 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(10, 10);
		c.lineTo(190, 190);
		c.stroke();
				
			c.beginPath();
		c.moveTo(190, 10);
		c.lineTo(10, 190);
		c.stroke();
		 a1 = 0
		 a1xx = 1;
		 fin()}
	}
	
	var A2X = function(){
		//A2
		if(a2 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(210, 10);
		c.lineTo(390, 190);
		c.stroke();
				
			c.beginPath();
		c.moveTo(390, 10);
		c.lineTo(210, 190);
		c.stroke();
		a2=0;
		a2xx = 1;
		fin();
		C2C();
		}
	}
	
	var A3X = function(){	
		//A3
		if(a3 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(410, 10);
		c.lineTo(590, 190);
		c.stroke();
				
			c.beginPath();
		c.moveTo(590, 10);
		c.lineTo(410, 190);
		c.stroke();
		a3=0;
		a3xx = 1;
		fin();
		C1C();
		}
	}
	
	var B1X = function(){
		//B1
		if(b1 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(10, 210);
		c.lineTo(190, 390);
		c.stroke();
				
			c.beginPath();
		c.moveTo(190, 210);
		c.lineTo(10, 390);
		c.stroke();
		b1=0;
		b1xx = 1;
		fin();
		C3C();
		}
	}
	
	var B2X = function(){
		//B2
		if(b2 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(210, 210);
		c.lineTo(390, 390);
		c.stroke();
				
			c.beginPath();
		c.moveTo(390, 210);
		c.lineTo(210, 390);
		c.stroke();
		b2=0;
		b2xx = 1;
		fin();
		A1C();
	}
	}
	
	var B3X = function(){	
		//B3
		if(b3 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(410, 210);
		c.lineTo(590, 390);
		c.stroke();
				
			c.beginPath();
		c.moveTo(590, 210);
		c.lineTo(410, 390);
		c.stroke();
		b3=0;
		b3xx = 1;
		fin();
		B2C();
		}
	}
	
	var C1X = function(){
		//C1
		if(c1 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(10, 410);
		c.lineTo(190, 590);
		c.stroke();
				
			c.beginPath();
		c.moveTo(190, 410);
		c.lineTo(10, 590);
		c.stroke();
		c1=0;
		c1xx = 1;
		fin();
		}
	}
	
	var C2X = function(){
		//C2
		if(c2 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(210, 410);
		c.lineTo(390, 590);
		c.stroke();
				
			c.beginPath();
		c.moveTo(390, 410);
		c.lineTo(210, 590);
		c.stroke();
		c2=0;
		c2xx = 1;
		fin();
		B3C();
		}
	}
	
	var C3X = function(){	
		//C3
		if(c3 == 1){
		var c = document.getElementById("pane").getContext('2d');
			c.beginPath();
		c.moveTo(410, 410);
		c.lineTo(590, 590);
		c.stroke();
				
			c.beginPath();
		c.moveTo(590, 410);
		c.lineTo(410, 590);
		c.stroke();
		c3=0;
		c3xx = 1;
		fin();
		}
	}
	
	var fin = function(){
		if(a1cx == 1 && a2cx == 1 && a3cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(b1cx == 1 && b2cx == 1 && b3cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(c1cx == 1 && c2cx == 1 && c3cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(a1cx == 1 && b1cx == 1 && c1cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(a2cx == 1 && b2cx == 1 && c2cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(a3cx == 1 && b3cx == 1 && c3cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(a1cx == 1 && b2cx == 1 && c3cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(a3cx == 1 && b2cx == 1 && c1cx == 1){
			alert("L'ordinateur a gagné");
			end = 1;
		}
		if(a1xx == 1 && a2xx == 1 && a3xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(b1xx == 1 && b2xx == 1 && b3xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(c1xx == 1 && c2xx == 1 && c3xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(a1xx == 1 && b1xx == 1 && c1xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(a2xx == 1 && b2xx == 1 && c2xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(a3xx == 1 && b3xx == 1 && c3xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(a1xx == 1 && b2xx == 1 && c3xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		if(a3xx == 1 && b2xx == 1 && c1xx == 1){
			alert("Le JOUEUR a gagné");
			end = 1;
		}
		
		if(end == 1){
			a1 = 0;
			a2 = 0;
			a3 = 0;
			b1 = 0;
			b2 = 0;
			b3 = 0;
			c1 = 0;
			c2 = 0;
			c3 = 0;
			}
	}
	
	window.addEventListener("load",abonnements);// lance la fonction abonnements après le chargement de ma page html